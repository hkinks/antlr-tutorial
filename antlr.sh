#!/bin/bash
###################################################################################################
# This is a script file for quickly setting up a workspace on a Linux machine.                    #
# It will download the specified ANTLR4 library, save it to lib folder, set it to classpath       #
# and make aliases for antlr4 and grun.                                                           #
#                                                                                                 #
# To run it: source antlr.sh                                                                      #
###################################################################################################
VER=4.5.1 #2014 4.4 was used
LIBNAME="antlr-$VER-complete"
mkdir lib
cd ./lib
wget http://antlr.org/download/$LIBNAME.jar
antlrpath="`pwd`/$LIBNAME.jar"
echo "Downloaded antlr4 libary to $antlrpath."
echo "export CLASSPATH='.:$antlrpath'" >> ~/.bashrc
echo "alias antlr4='java -jar $antlrpath'" >> ~/.bashrc
echo "alias grun='java org.antlr.v4.runtime.misc.TestRig'" >> ~/.bashrc
echo "Created 'antlr4' and 'grun' aliases." 
source ~/.bashrc
cd ..