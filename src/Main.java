import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.List;
import java.util.Map;

/**
 * Main function.
 * If SHOW_TREE is set to true, parse tree will not be walked, instead we visualize the tree
 * @author Hannes Kinks
 * @since 11/5/14
 */
public class Main {
    public static final boolean SHOW_TREE = false;
    public static final String DEFAULT_INPUTFILE = "input.csv";
    public static final String DEFAULT_OUTPUTFILE = "output.json";

    public static void main(String[] args) throws Exception {
        String inputFile = DEFAULT_INPUTFILE;
        if ( args.length>0 ) inputFile = args[0]; // if arguments supplied get input file from argument

        InputStream is = new FileInputStream(inputFile); // input from file
        CSVLexer lexer = new CSVLexer(new ANTLRInputStream(is));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CSVParser parser = new CSVParser(tokens);

        if (SHOW_TREE) {
            /* show parse tree in gui */
            ParserRuleContext ruleContext = parser.file();
            Trees.inspect(ruleContext, parser); // from version 4.5.1
//            ruleContext.inspect(parser); // for older versions
        } else {
            parser.setBuildParseTree(true); // tell ANTLR to build a parse tree
            ParseTree tree = parser.file();

            /* walk the tree */
            ParseTreeWalker walker = new ParseTreeWalker();
            Loader loader = new Loader();
            walker.walk(loader, tree);

            /* convert to json and make a string out of it*/
            JSONArray jsonArray = new JSONArray();
            for (Map<String, String> row : loader.rows) {
                JSONObject jsonObject = new JSONObject(row);
                jsonArray.put(jsonObject);
            }
            int indentation = 2;
            String jsonText = jsonArray.toString(indentation);


            /* output file */
            FileWriter fileWriter = new FileWriter(DEFAULT_OUTPUTFILE);
            fileWriter.write(jsonText);
            fileWriter.close();

        }
    }
}
