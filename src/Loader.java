import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Hannes Kinks
 * @since 11/5/14
 */
public class Loader extends CSVBaseListener {
    public static final String EMPTY = "";
    /** Load a list of row maps that map field name to value */
    List<Map<String,String>> rows = new ArrayList<Map<String, String>>();
    /** List of column names */
    List<String> header;
    /** Build up a list of fields in current row */
    List<String> currentRowFieldValues;

    public void exitHeader(CSVParser.HeaderContext ctx) {
        header = new ArrayList<String>();
        header.addAll(currentRowFieldValues);
    }

    public void enterLine(CSVParser.LineContext ctx) {
        currentRowFieldValues = new ArrayList<String>();
    }

    public void exitLine(CSVParser.LineContext ctx) {
        // If this is the header row, do nothing
        // if ( ctx.parent instanceof CSVParser.HdrContext ) return; OR:
        if ( ctx.getParent().getRuleIndex() == CSVParser.RULE_header ) return;
        // It's a data row
        Map<String, String> m = new LinkedHashMap<String, String>();
        int i = 0;
        for (String v : currentRowFieldValues) {
            m.put(header.get(i), v);
            i++;
        }
        rows.add(m);
    }

    public void exitText(CSVParser.TextContext ctx) {
        currentRowFieldValues.add(ctx.TEXT().getText());
    }

    public void exitEmpty(CSVParser.EmptyContext ctx) {
        currentRowFieldValues.add(EMPTY);
    }
}
