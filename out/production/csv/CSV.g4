grammar CSV;

file : header line* lastline;
header : line ;

line : entry (',' entry)* eol;
lastline : entry (',' entry)* eol?;

eol : '\r'? '\n' ;

entry
    :   TEXT        # text
    |   STRING      # string
    |               # empty
    ;

TEXT : ~[,\n\r"]+ ;
STRING : '"' ('""'|~'"')* '"' ;
WS : [ ]+ -> skip;