grammar CSV;

file: header NL (line NL)+ line NL?;
header: line;

line: entry (SEPARATOR entry)*;

entry: TEXT     #text
     |          #empty
     ;

NL: '\r'? '\n';
SEPARATOR: ',';
TEXT: ~[,\n\r"]+;
WS: [ ]+ -> skip;
